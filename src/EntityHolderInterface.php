<?php

namespace Drupal\entity_holder;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a entity holder entity type.
 */
interface EntityHolderInterface extends ConfigEntityInterface {

  /**
   * Gets the held entity type ID.
   *
   * @return string
   *   The held entity type ID.
   */
  public function getHeldEntityTypeId();

  /**
   * Gets the held entity type bundle.
   *
   * @return string
   *   The held entity type bundle.
   */
  public function getHeldEntityBundle();

  /**
   * Gets the held entity UUID.
   *
   * @return null|string
   *   The held entity UUID, NULL if not set.
   */
  public function getHeldEntityUuid();

  /**
   * Gets the held entity canonical path.
   *
   * @return null|string
   *   The held entity path, NULL if not set.
   */
  public function getHeldEntityPath();

  /**
   * Gets the held entity entity.
   *
   * @return null|\Drupal\Core\Entity\ContentEntityInterface
   *   The held entity entity, NULL if not found.
   */
  public function getHeldEntity();

  /**
   * Sets the given entity as held by this holder.
   *
   * @param null|\Drupal\Core\Entity\ContentEntityInterface $entity
   *   Content entity to be held. NULL to clear the holder.
   *
   * @return self
   *   The called entity holder.
   *
   * @throws \InvalidArgumentException
   *   If type or bundle of the given entity do not match the holder expected.
   */
  public function holdEntity(?ContentEntityInterface $entity);

  /**
   * Gets the entity holder path.
   *
   * @return string
   *   The entity holder path.
   */
  public function getPath();

  /**
   * Gets the entity holder description.
   *
   * @return string
   *   The entity holder description.
   */
  public function getDescription();

  /**
   * Gets the entity holder title.
   *
   * @return string
   *   The entity holder title.
   */
  public function getTitle();

  /**
   * Gets the fallback content.
   *
   * @return null|array
   *   The fallback content render array, NULL if empty.
   */
  public function getFallbackContent();

}
