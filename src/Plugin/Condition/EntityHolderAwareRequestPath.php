<?php

namespace Drupal\entity_holder\Plugin\Condition;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\system\Plugin\Condition\RequestPath;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Overrides the core request path condition plugin.
 */
class EntityHolderAwareRequestPath extends RequestPath {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a RequestPath condition plugin.
   *
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   An alias manager to find the alias for the current system path.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path matcher service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The current route match.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(AliasManagerInterface $alias_manager, PathMatcherInterface $path_matcher, RequestStack $request_stack, CurrentPathStack $current_path, EntityTypeManagerInterface $entity_type_manager, RouteMatchInterface $current_route_match, array $configuration, $plugin_id, array $plugin_definition) {
    parent::__construct($alias_manager, $path_matcher, $request_stack, $current_path, $configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('path_alias.manager'),
      $container->get('path.matcher'),
      $container->get('request_stack'),
      $container->get('path.current'),
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $configuration,
      $plugin_id,
      $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    return parent::evaluate() || $this->evaluateHolderPath();
  }

  /**
   * Evaluates condition against holder path if found.
   *
   * @return bool
   *   TRUE if in a holder managed URL and matches condition.
   */
  protected function evaluateHolderPath() {
    // Search for content entity in current route parameters.
    $content_entity = NULL;
    foreach ($this->routeMatch->getParameters() as $param) {
      if ($param instanceof ContentEntityInterface) {
        $content_entity = $param;
        break;
      }
    }

    if (!$content_entity) {
      return FALSE;
    }

    // Search for the entity holder for current route content entity.
    /** @var \Drupal\entity_holder\EntityHolderInterface[] $entity_holders */
    $entity_holders = $this->entityTypeManager->getStorage('entity_holder')->loadByProperties(['held_entity_uuid' => $content_entity->uuid()]);
    if (!$entity_holders) {
      // Not in an page managed by Entity Holder.
      return FALSE;
    }
    $entity_holder = reset($entity_holders);

    // As the parent class, convert path to lowercase. This allows comparison of
    // the same path with different case. Ex: /Page, /page, /PAGE.
    $pages = mb_strtolower($this->configuration['pages']);
    return $this->pathMatcher->matchPath($entity_holder->getPath(), $pages);
  }

}
