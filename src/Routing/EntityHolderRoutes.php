<?php

namespace Drupal\entity_holder\Routing;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\entity_holder\Controller\EntityHolderController;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Provides routes for entity holder entities.
 */
class EntityHolderRoutes extends RouteSubscriberBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * Constructs a new EntityHolderRoutes.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   The cache tags invalidator.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CacheTagsInvalidatorInterface $cache_tags_invalidator) {
    $this->entityTypeManager = $entity_type_manager;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $entity_holder_storage = $this->entityTypeManager->getStorage('entity_holder');
    foreach ($entity_holder_storage->loadMultiple() as $entity_id => $entity_holder) {
      /** @var \Drupal\entity_holder\EntityHolderInterface $entity_holder */
      // If the entity holder is disabled skip making a route for it.
      if (!$entity_holder->status()) {
        continue;
      }

      $defaults = [
        '_title_callback' => EntityHolderController::class . '::entityHolderTitle',
        '_controller' => EntityHolderController::class . '::entityHolderView',
        'entity_holder' => $entity_id,
      ];

      $requirements = [
        '_entity_access' => 'entity_holder.view',
      ];

      $parameters = [
        'entity_holder' => [
          'type' => 'entity:entity_holder',
        ],
      ];

      $route = new Route(
        $entity_holder->getPath(),
        $defaults,
        $requirements,
        [
          'parameters' => $parameters,
        ]
      );

      $collection->add('entity_holder.view.' . $entity_id, $route);

      // Invalidate any page with the same base route name. See
      // \Drupal\page_manager\EventSubscriber\RouteNameResponseSubscriber.
      // @todo required? $this->cacheTagsInvalidator->invalidateTags(["page_manager_route_name:$base_route_name"]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // Run after EntityRouteAlterSubscriber.
    $events[RoutingEvents::ALTER][] = ['onAlterRoutes', -160];
    return $events;
  }

}
