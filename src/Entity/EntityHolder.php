<?php

namespace Drupal\entity_holder\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\entity_holder\EntityHolderInterface;

/**
 * Defines the entity holder entity type.
 *
 * @ConfigEntityType(
 *   id = "entity_holder",
 *   label = @Translation("Entity Holder"),
 *   label_collection = @Translation("Entity Holders"),
 *   label_singular = @Translation("entity holder"),
 *   label_plural = @Translation("entity holders"),
 *   label_count = @PluralTranslation(
 *     singular = "@count entity holder",
 *     plural = "@count entity holders",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\entity_holder\EntityHolderStorage",
 *     "list_builder" = "Drupal\entity_holder\EntityHolderListBuilder",
 *     "access" = "Drupal\entity_holder\Entity\EntityHolderAccess",
 *     "form" = {
 *       "add" = "Drupal\entity_holder\Form\EntityHolderForm",
 *       "edit" = "Drupal\entity_holder\Form\EntityHolderForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   admin_permission = "administer entity holders",
 *   links = {
 *     "collection" = "/admin/structure/entity-holder",
 *     "add-form" = "/admin/structure/entity-holder/add",
 *     "edit-form" = "/admin/structure/entity-holder/{entity_holder}",
 *     "delete-form" = "/admin/structure/entity-holder/{entity_holder}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "title",
 *     "path",
 *     "fallback_content",
 *     "held_entity_type",
 *     "held_entity_bundle",
 *     "held_entity_uuid"
 *   }
 * )
 */
class EntityHolder extends ConfigEntityBase implements EntityHolderInterface {

  /**
   * The entity holder ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The entity holder administrative label.
   *
   * @var string
   */
  protected $label;

  /**
   * The entity holder path.
   *
   * @var string
   */
  protected $path;

  /**
   * The entity_holder administrative description.
   *
   * @var string
   */
  protected $description;

  /**
   * The entity holder title.
   *
   * @var string
   */
  protected $title;

  /**
   * The fallback content.
   *
   * @var text_format
   */
  protected $fallback_content;

  /**
   * The held entity type.
   *
   * @var string
   */
  protected $held_entity_type;

  /**
   * The held entity bundle.
   *
   * @var string
   */
  protected $held_entity_bundle;

  /**
   * The held entity UUID.
   *
   * @var string
   */
  protected $held_entity_uuid;

  /**
   * The entity holder status.
   *
   * @var bool
   */
  protected $status = TRUE;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeldEntity() {
    if (empty($this->held_entity_uuid)) {
      return NULL;
    }

    $entities = $this->entityTypeManager()->getStorage($this->held_entity_type)
      ->loadByProperties(['uuid' => $this->held_entity_uuid]);

    return reset($entities) ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function holdEntity(?ContentEntityInterface $entity) {
    if (!$entity) {
      // Holder clean up.
      $this->held_entity_uuid = NULL;
      return $this;
    }

    if ($this->getHeldEntityTypeId() != $entity->getEntityTypeId()) {
      throw new \InvalidArgumentException('Entity type mismatch.');
    }

    $expected_bundle = $this->getHeldEntityBundle();
    if (!empty($expected_bundle) && $expected_bundle != $entity->bundle()) {
      throw new \InvalidArgumentException('Entity bundle mismatch.');
    }

    $this->held_entity_uuid = $entity->uuid();

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeldEntityTypeId() {
    return $this->held_entity_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeldEntityBundle() {
    return $this->held_entity_bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeldEntityUuid() {
    return $this->held_entity_uuid ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeldEntityPath() {
    $held_entity = $this->getHeldEntity();
    return $held_entity
      ? $held_entity->toUrl('canonical', ['alias' => TRUE])->toString()
      : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getPath() {
    return $this->path;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackContent() {
    $build = NULL;
    if (!empty($this->fallback_content['value'])) {
      $build['content'] = [
        '#type' => 'processed_text',
        '#text' => $this->fallback_content['value'],
        '#format' => $this->fallback_content['format'],
      ];
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();

    // Adds dependency on the held entity type provider.
    $held_entity_type_def = $this->entityTypeManager()->getDefinition($this->getHeldEntityTypeId());
    $this->addDependency('module', $held_entity_type_def->getProvider());

    // Adds dependency on the held entity bundle config item.
    $held_entity_bundle_config_dependency = $held_entity_type_def->getBundleConfigDependency($this->getHeldEntityBundle());
    $this->addDependency($held_entity_bundle_config_dependency['type'], $held_entity_bundle_config_dependency['name']);

    return $this;
  }

}
