<?php

namespace Drupal\entity_holder\Form;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Entity Holder form.
 *
 * @property \Drupal\entity_holder\EntityHolderInterface $entity
 */
class EntityHolderForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Administrative label for the entity holder.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\entity_holder\Entity\EntityHolder::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->getDescription(),
      '#description' => $this->t('Administrative description of the entity holder.'),
      '#rows' => 3,
    ];

    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Holder path'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->getPath(),
      '#description' => $this->t('Absolute path of this entity holder. Ending slash is automatically removed.'),
      '#required' => TRUE,
      '#element_validate' => [[$this, 'validatePath']],
    ];

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Holder title'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->getTitle(),
      '#description' => $this->t('Displayed along with the fallback content if the held entity is not found. Default held entity title on creation.'),
      '#required' => TRUE,
    ];

    $fallback_content = $this->entity->get('fallback_content');
    $form['fallback_content'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Fallback content'),
      '#default_value' => $fallback_content['value'] ?? '',
      '#format' => $fallback_content['format'] ?? 'plain_text',
      '#description' => $this->t('Page content if no entity found. Leave empty to return a page not found (404) status response.'),
    ];

    // Build options for the entity type selector.
    $entity_types = $this->getElegibleEntityTypes();
    $options = [];
    foreach ($entity_types as $entity_type_id => $entity_type) {
      $options[$entity_type_id] = $entity_type->getLabel();
    }
    asort($options);

    $held_entity_type_id = $this->entity->getHeldEntityTypeId();
    $form['held_entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity type'),
      '#description' => $this->t('The held entity type. Only entity types that defines a canonical route are eligible.'),
      '#options' => $options,
      '#default_value' => $held_entity_type_id,
      '#required' => TRUE,
    ];

    foreach ($entity_types as $entity_type_id => $entity_type) {
      if ($entity_type_bundle = $entity_type->getBundleEntityType()) {
        $entity_bundles = $this->entityTypeManager
          ->getStorage($entity_type_bundle)->loadMultiple();
        $options = [];
        foreach ($entity_bundles as $entity_bundle) {
          $options[$entity_bundle->id()] = $entity_bundle->label();
        }
        asort($options);

        $form[$entity_type_id . '_entity_bundle'] = [
          '#type' => 'select',
          '#title' => $this->t('Entity bundle'),
          '#description' => $this->t('The held entity bundle.'),
          '#options' => $options,
          '#default_value' => $entity_type_id == $held_entity_type_id ? $this->entity->getHeldEntityBundle() : NULL,
          '#states' => [
            'visible' => [
              'select[name="held_entity_type"]' => [
                'value' => $entity_type_id,
              ],
            ],
            'required' => [
              'select[name="held_entity_type"]' => [
                'value' => $entity_type_id,
              ],
            ],
          ],
        ];
      }

      $form[$entity_type_id . '_entity'] = [
        '#type' => 'entity_autocomplete',
        '#title' => $entity_type->getLabel(),
        '#description' => $this->t('Existing entity to be used as the held entity.'),
        '#target_type' => $entity_type_id,
        '#default_value' => $entity_type_id == $held_entity_type_id ? $this->entity->getHeldEntity() : NULL,
        '#states' => [
          'visible' => [
            'select[name="held_entity_type"]' => [
              'value' => $entity_type_id,
            ],
          ],
        ],
      ];
    }

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    return $form;
  }

  /**
   * Get a list of entity types that can be held.
   *
   * @return array
   *   An array of entity types keyed by their IDs.
   */
  protected function getElegibleEntityTypes(): array {
    $entity_types = [];
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if ($entity_type instanceof ContentEntityTypeInterface
        && $entity_type->hasRouteProviders()
        // Limit eligible entity types to those with canonical route.
        && $entity_type->hasLinkTemplate('canonical')
        // And the canonical route is not the same as for the edit form.
        && $entity_type->getLinkTemplate('canonical') !== $entity_type->getLinkTemplate('edit-form')) {
        $entity_types[$entity_type_id] = $entity_type;
      }
    }

    return $entity_types;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $held_entity_type_id = $form_state->getValue('held_entity_type');
    $held_entity_bundle = $form_state->getValue($held_entity_type_id . '_entity_bundle');
    if ($held_entity_bundle &&
      ($held_entity = $this->getEntityToHold($form_state))
      && $held_entity->bundle() != $held_entity_bundle) {
      // The selected entity must has the same bundle as the held entity.
      $held_entity_bundle_id = $this->entityTypeManager->getDefinition($held_entity_type_id)->getBundleEntityType();
      $held_entity_bundle = $this->entityTypeManager->getStorage($held_entity_bundle_id)->load($held_entity_bundle);
      $form_state->setErrorByName($held_entity_type_id . '_entity', $this->t('The selected entity is not a %bundle', ['%bundle' => $held_entity_bundle->label()]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\entity_holder\EntityHolderInterface $entity_holder */
    $entity_holder = parent::buildEntity($form, $form_state);
    $entity_holder->set('held_entity_bundle', $form_state->getValue($entity_holder->getHeldEntityTypeId() . '_entity_bundle'));
    $entity_holder->holdEntity($this->getEntityToHold($form_state));

    return $entity_holder;
  }

  /**
   * Gets the referenced entity on form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return null|\Drupal\Core\Entity\EntityInterface
   *   The referenced entity if found in form state values.
   */
  protected function getEntityToHold(FormStateInterface $form_state) {
    $held_entity = NULL;
    $held_entity_type_id = $form_state->getValue('held_entity_type');
    $held_entity_reference_value = $form_state->getValue($held_entity_type_id . '_entity');
    if (!empty($held_entity_reference_value)) {
      $held_entity_id = is_numeric($held_entity_reference_value)
        ? $held_entity_reference_value
        : EntityAutocomplete::extractEntityIdFromAutocompleteInput($held_entity_reference_value);
      $held_entity = $this->entityTypeManager->getStorage($held_entity_type_id)
        ->load($held_entity_id);
    }

    return $held_entity;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new entity holder %label.', $message_args)
      : $this->t('Updated entity holder %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePath(&$element, FormStateInterface $form_state) {
    if ($value = trim($element['#value'], '/')) {
      // Ensure the path has a leading slash.
      $value = '/' . $value;
      $form_state->setValueForElement($element, $value);

      // Ensure each path is unique.
      $path_query = $this->entityTypeManager->getStorage('entity_holder')->getQuery()
        ->accessCheck(TRUE)
        ->condition('path', $value);
      if (!$this->entity->isNew()) {
        $path_query->condition('id', $this->entity->id(), '<>');
      }
      $path = $path_query->execute();
      if ($path) {
        $form_state->setErrorByName('path', $this->t('The entity holder path must be unique.'));
      }
    }
    else {
      // Check to make sure the path exists after stripping slashes.
      $form_state->setErrorByName('path', $this->t("Path is required."));
    }
  }

}
