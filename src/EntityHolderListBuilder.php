<?php

namespace Drupal\entity_holder;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of entity holders.
 */
class EntityHolderListBuilder extends ConfigEntityListBuilder {

  /**
   * Our entity helper.
   *
   * @var \Drupal\entity_holder\EntityHolderEntityHelper
   */
  protected $entityHelper;

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\entity_holder\EntityHolderEntityHelper $entity_helper
   *   Our entity helper.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, EntityHolderEntityHelper $entity_helper) {
    parent::__construct($entity_type, $storage);
    $this->entityHelper = $entity_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('entity_holder.entity_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['description'] = $this->t('Description');
    $header['path'] = $this->t('Path');
    $header['entity'] = $this->t('Held entity');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\entity_holder\EntityHolderInterface $entity */
    /** @var \Drupal\Core\Entity\ContentEntityInterface $held_entity */
    $held_entity = $entity->getHeldEntity();

    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['description'] = $entity->getDescription();
    $path = $entity->get('path');
    $row['path'] = Link::fromTextAndUrl($path, Url::fromUserInput($path));
    $row['entity'] = $held_entity ? $held_entity->toLink() : $this->t('- None -');
    $row['status'] = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    /** @var \Drupal\entity_holder\EntityHolderInterface $entity */
    $operations = [];

    if (!$entity->getHeldEntity()) {
      $create_url = $this->entityHelper->holdEntityCreateFormUrl($entity);
      if ($create_url->access()) {
        // Add the create held entity operation.
        $operations = [
          'create_held_entity' => [
            'title' => $this->t('Create held entity'),
            'weight' => 0,
            'url' => $create_url,
          ],
        ];
      }
    }

    return $operations + parent::getDefaultOperations($entity);
  }

}
