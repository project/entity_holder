<?php

namespace Drupal\entity_holder;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;

/**
 * Entity Holder entity helper.
 */
class EntityHolderEntityHelper {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * EntityHolderEntityHelper constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   The UUID service.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $destination
   *   The redirect destination service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, UuidInterface $uuid_service, RedirectDestinationInterface $destination) {
    $this->entityTypeManager = $entity_type_manager;
    $this->uuidService = $uuid_service;
    $this->redirectDestination = $destination;
  }

  /**
   * Check access to the held entity creation form.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account.
   * @param \Drupal\entity_holder\EntityHolderInterface $entity_holder
   *   The entity holder.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Allowed if the user has permission to create the held entity.
   */
  public function checkHeldEntityFormAccess(AccountInterface $account, EntityHolderInterface $entity_holder) {
    $held_entity_access_handler = $this->entityTypeManager->getAccessControlHandler($entity_holder->getHeldEntityTypeId());
    if ($held_entity_access_handler->createAccess($entity_holder->getHeldEntityBundle())) {
      return AccessResult::allowed();
    }

    // Deny access if the user has no permission to create the held entity.
    return AccessResult::forbidden();
  }

  /**
   * Composes URL to the form that creates a content entity for a holder.
   *
   * @param \Drupal\entity_holder\EntityHolderInterface $entity_holder
   *   The entity holder.
   *
   * @return \Drupal\Core\Url
   *   URL to entity holder content creation form.
   *
   * @see \Drupal\entity_holder\Form\EntityHolderForm
   */
  public function holdEntityCreateFormUrl(EntityHolderInterface $entity_holder) {
    $uuid = $entity_holder->getHeldEntityUuid() ?: $this->uuidService->generate();
    // URL to the form.
    $create_url = Url::fromRoute('entity.entity_holder.held_entity_form', [
      'entity_holder' => $entity_holder->id(),
      'uuid' => $uuid,
    ]);

    // Sets the hold entity controller method as destination.
    $this->redirectDestination->set(Url::fromRoute('entity.entity_holder.held_entity', [
      'entity_holder' => $entity_holder->id(),
      'uuid' => $uuid,
    ])->toString());
    $create_url->mergeOptions(['query' => $this->redirectDestination->getAsArray()]);

    return $create_url;
  }

}
