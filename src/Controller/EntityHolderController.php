<?php

namespace Drupal\entity_holder\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\entity_holder\EntityHolderEntityHelper;
use Drupal\entity_holder\EntityHolderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Returns responses for Entity Holder routes.
 */
class EntityHolderController extends ControllerBase {

  /**
   * Symfony\Component\HttpKernel\HttpKernelInterface definition.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The entity form builder.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilderInterface
   */
  protected $entityFormBuilder;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Our entity helper.
   *
   * @var \Drupal\entity_holder\EntityHolderEntityHelper
   */
  protected $entityHelper;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * EntityHolderController constructor.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The HTTP kernel.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Entity\EntityFormBuilderInterface $entity_form_builder
   *   The entity form builder.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\entity_holder\EntityHolderEntityHelper $entity_helper
   *   Our entity helper.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(HttpKernelInterface $http_kernel, RendererInterface $renderer, EntityFormBuilderInterface $entity_form_builder, EntityRepositoryInterface $entity_repository, EntityHolderEntityHelper $entity_helper, RequestStack $request_stack) {
    $this->httpKernel = $http_kernel;
    $this->renderer = $renderer;
    $this->entityFormBuilder = $entity_form_builder;
    $this->entityRepository = $entity_repository;
    $this->entityHelper = $entity_helper;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_kernel.basic'),
      $container->get('renderer'),
      $container->get('entity.form_builder'),
      $container->get('entity.repository'),
      $container->get('entity_holder.entity_helper'),
      $container->get('request_stack')
    );
  }

  /**
   * Builds the response.
   */
  public function buildHeldEntityForm(EntityHolderInterface $entity_holder, string $uuid) {
    if ($held_entity = $entity_holder->getHeldEntity()) {
      // If the held entity already exists, redirect to its edit form.
      return new TrustedRedirectResponse($held_entity->toUrl('edit-form')->setAbsolute(TRUE)->toString(TRUE));
    }

    // Build the held entity create form.
    $held_entity_type_storage = $this->entityTypeManager()
      ->getStorage($entity_holder->getHeldEntityTypeId());
    $held_entity_type = $held_entity_type_storage->getEntityType();
    $held_entity = $held_entity_type_storage->create([
      'uuid' => $uuid,
      $held_entity_type->getKey('bundle') => $entity_holder->getHeldEntityBundle(),
      // Set the holder title as default title for the the held entity.
      $held_entity_type->getKey('label') => $entity_holder->getTitle(),
    ]);

    $form = $this->entityFormBuilder->getForm($held_entity);
    $form['#title'] = $this->t('Create @name', ['@name' => $held_entity_type->getLabel()]);

    return $form;
  }

  /**
   * Entity holder route title callback.
   *
   * @param \Drupal\entity_holder\EntityHolderInterface $entity_holder
   *   The entity holder.
   *
   * @return array
   *   The held entity title as render array, the entity holder title if there
   *   is no held entity.
   */
  public function entityHolderTitle(EntityHolderInterface $entity_holder) {
    $held_entity = $entity_holder->getHeldEntity();

    return [
      '#markup' => $held_entity ? $held_entity->label() : $entity_holder->getTitle(),
      '#allowed_tags' => Xss::getHtmlTagList(),
    ];
  }

  /**
   * Build response to entity holder path request.
   *
   * @param \Drupal\entity_holder\EntityHolderInterface $entity_holder
   *   The entity holder.
   *
   * @return array
   *   Render array of the held entity or fallback content.
   *
   * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
   *   If there is no held entity nor fallback content and the user cannot
   *   create the hosted entity.
   */
  public function entityHolderView(EntityHolderInterface $entity_holder) {
    if ($held_entity = $entity_holder->getHeldEntity()) {
      // Compose response from subrequest to the held entity canonical URL.
      $context = new RenderContext();
      $current_request = $this->requestStack->getCurrentRequest();
      $response = $this->renderer->executeInRenderContext($context, function () use ($held_entity, $current_request) {
        $sub_request = Request::create($held_entity->toUrl()->setAbsolute()->toString(), 'GET', $current_request->query->all(), $current_request->cookies->all(), [], $current_request->server->all());
        $sub_request->setSession($current_request->getSession());
        return $this->httpKernel->handle($sub_request, HttpKernelInterface::SUB_REQUEST);
      });

      if (!$context->isEmpty()) {
        $metadata = $context->pop();
        // Add entity holder cache information.
        $metadata->addCacheableDependency($entity_holder);
        $response->addCacheableDependency($metadata);
        $response->addAttachments($metadata->getAttachments());
      }

      return $response;
    }

    // Response with the fallback content if set.
    $build = $entity_holder->getFallbackContent() ?: [];

    // Set message the user.
    $create_url = $this->entityHelper->holdEntityCreateFormUrl($entity_holder);
    if ($create_url->access()) {
      $this->messenger()->addWarning($this->t('The content associated to this entity holder does not exist yet. <a href=":url">Create this content</a>.', [':url' => $create_url->toString()]));
    }
    elseif (empty($build)) {
      // There is no fallback content and the user cannot create the hosted
      // entity, return page not found.
      throw new NotFoundHttpException();
    }

    return $build;
  }

  /**
   * Holds content in an entity holder.
   *
   * Tipically called after content entity creation form submission.
   *
   * @param \Drupal\entity_holder\EntityHolderInterface $entity_holder
   *   The entity holder.
   * @param string $uuid
   *   UUID of the content entity to be held.
   *
   * @see \Drupal\entity_holder\EntityHolderListBuilder
   * @see \Drupal\entity_holder\Form\EntityHolderForm
   */
  public function holdEntity(EntityHolderInterface $entity_holder, string $uuid) {
    $content_entity = $this->entityRepository->loadEntityByUuid($entity_holder->getHeldEntityTypeId(), $uuid);
    if ($content_entity) {
      $entity_holder->holdEntity($content_entity);
      $entity_holder->save();
    }

    return $this->redirect('entity.entity_holder.collection');
  }

}
